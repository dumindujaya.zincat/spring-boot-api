package com.example.springbootapi.common.entity;

import jakarta.persistence.*;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "log_type")
public class LogType {
    @Id
    @Column(name = "id", length = 4)
    private String id;

    @Column(name = "type")
    private String type;

    @OneToMany(mappedBy = "type", cascade = CascadeType.ALL)
    private List<Log> log;
}
