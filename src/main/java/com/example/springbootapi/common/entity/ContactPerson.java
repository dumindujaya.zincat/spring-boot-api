package com.example.springbootapi.common.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "contact_person")
public class ContactPerson {
    @Id
    @Column(name = "id", length = 9)
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "designation")
    private String designation;

    @Column(name = "mobile", length = 15)
    private String mobile;

    @Column(name = "email", length = 250)
    private String email;

    @ManyToMany(mappedBy = "contactPersons")
    private List<Customer> customer;

}
