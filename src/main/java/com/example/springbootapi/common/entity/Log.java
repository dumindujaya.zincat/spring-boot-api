package com.example.springbootapi.common.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Date;
import java.time.DateTimeException;

@Data
@Entity
@Table(name = "log")
public class Log {

    @Id
    @GeneratedValue
    @Column(name = "id", length = 9)
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "type", referencedColumnName = "id")
    private LogType type;

    @ManyToOne(optional = false)
    @JoinColumn(name = "customer", referencedColumnName = "id")
    private Customer customer;

    @Column(name = "logged_at", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
    @Temporal(TemporalType.DATE)
    private Date timestamp;

}
