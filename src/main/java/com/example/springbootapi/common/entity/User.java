package com.example.springbootapi.common.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "user")
public class User {
    @Id
    @Column(name = "id", length = 4)
    private String id;

    @Column(name = "first_name")
    private String fName;

    @Column(name = "Last_name")
    private String lName;

    @Column(name = "user_name", length = 10, unique = true)
    private String username;

    @Column(name = "password", length = 8)
    private String pwd;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Log> log;
}
