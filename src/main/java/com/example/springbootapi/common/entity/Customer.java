package com.example.springbootapi.common.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @Column(name="id", length=9)
    private String id;

    @ManyToOne
    @JoinColumn(name = "type", referencedColumnName = "id")
    private CustomerType type;

    @Column(name = "code", nullable = false, unique = true, length =4)
    private String code;

    @Column(name = "ref_no")
    private String ref_no;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "company_name")
    private String company;

    @Column(name = "billing_address", nullable = false)
    private String address;

    @Column(name = "nic_no", unique = true)
    private String nic;

    @Column(name = "mobile", nullable = false)
    private String mobile;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "city")
    private String city;

    @Column(name = "gender")
    private String gender;

    @Column(name = "alive")
    private boolean alive = true;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Log> logs;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Document> documents;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "Customer_contact_person",
            joinColumns = @JoinColumn(name = "customer_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "contact_person_id", referencedColumnName = "id"))
    private  List<ContactPerson> contactPersons;

}
