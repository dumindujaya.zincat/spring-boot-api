package com.example.springbootapi.common.response;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class GenericResponse {

        private HttpStatus status;

        private Object response;

}

