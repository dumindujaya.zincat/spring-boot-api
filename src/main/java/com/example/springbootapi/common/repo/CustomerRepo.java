package com.example.springbootapi.common.repo;

import com.example.springbootapi.common.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface CustomerRepo extends JpaRepository<Customer,String> {
    Customer getCustomerByCode(String code);

    @Query(nativeQuery = true, value = "SELECT * FROM customer c WHERE c.code LIKE %:key% OR c.name LIKE %:key% OR c.city LIKE %:key% OR c.gender LIKE %:key%")
    List<Customer> filterCustomers(@Param("key") String key);

    @Query(nativeQuery = true, value = "SELECT code FROM customer ORDER BY code DESC LIMIT 1")
    String getLastCode();
}
