package com.example.springbootapi.common.repo;

import com.example.springbootapi.common.entity.ContactPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactPersonRepo extends JpaRepository<ContactPerson,String> {
    boolean existsByEmail(String mail);
}
