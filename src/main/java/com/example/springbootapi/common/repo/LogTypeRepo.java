package com.example.springbootapi.common.repo;

import com.example.springbootapi.common.entity.LogType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogTypeRepo extends JpaRepository<LogType,String> {
    LogType getLogTypeById(String id);
}
