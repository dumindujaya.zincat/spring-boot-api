package com.example.springbootapi.common.repo;

import com.example.springbootapi.common.entity.CustomerType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerTypeRepo extends JpaRepository<CustomerType,String> {
    CustomerType getCustomerTypeById(String id);
}
