package com.example.springbootapi.common.repo;

import com.example.springbootapi.common.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentRepo extends JpaRepository<Document,String> {
}
