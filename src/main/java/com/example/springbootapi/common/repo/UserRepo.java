package com.example.springbootapi.common.repo;

import com.example.springbootapi.common.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User,String> {
    User getUserById(String id);
}
