package com.example.springbootapi.common.repo;

import com.example.springbootapi.common.entity.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepo extends JpaRepository<Log,Integer> {
}
