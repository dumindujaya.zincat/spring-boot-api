package com.example.springbootapi.customer.dto;

import com.example.springbootapi.common.entity.Customer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class DocumentDTO {

    private String id;

    private String name;

    private String type;

    private byte[] data;

    @JsonIgnore
    private CustomerDTO customer;
}
