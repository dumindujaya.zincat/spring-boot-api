package com.example.springbootapi.customer.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;

@Data
public class CustomerTypeDTO {
    private String id;
    private String type;

    @JsonIgnore
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private ArrayList<CustomerDTO> customerDTOs;

}
