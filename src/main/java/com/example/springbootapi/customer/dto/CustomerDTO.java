package com.example.springbootapi.customer.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public class CustomerDTO {

    private CustomerTypeDTO type;

    private String id;

    private String code;

    private String ref_no;

    private String name;

    private String company;

    private String address;

    private String mobileNo;

    private String nic;

    private String email;

    private String country;

    private String city;

    private String gender;

    private String userId;

    private List<DocumentDTO> documents;

    @JsonIgnore
    private List<LogDTO> logs;


    private  List<ContactPersonDTO> contactPersons;

}
