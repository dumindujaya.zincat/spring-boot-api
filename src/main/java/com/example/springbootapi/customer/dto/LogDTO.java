package com.example.springbootapi.customer.dto;

import java.sql.Date;

public class LogDTO {
    private long id;

    private UserDTO userDTO;

    private LogTypeDTO logTypeDTO;

    private CustomerDTO customerDTO;

    private Date timestamp;
}
