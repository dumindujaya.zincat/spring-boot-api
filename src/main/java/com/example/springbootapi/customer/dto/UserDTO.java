package com.example.springbootapi.customer.dto;


import java.util.ArrayList;

public class UserDTO {

    private String id;

    private String fName;

    private String lName;

    private String username;

    private String pwd;

    private ArrayList<LogDTO> logDTOs;
}
