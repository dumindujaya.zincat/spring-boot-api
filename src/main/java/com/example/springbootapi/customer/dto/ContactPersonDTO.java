package com.example.springbootapi.customer.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.ArrayList;

@Data
public class ContactPersonDTO {

    private String id;
    private String name;
    private String designation;
    private String mobile;
    private String email;

    @JsonIgnore
    private ArrayList<CustomerDTO> customerDTOS;
}
