package com.example.springbootapi.customer.controller;

import com.example.springbootapi.common.response.GenericResponse;
import com.example.springbootapi.customer.dto.CustomerDTO;
import com.example.springbootapi.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/customer")
public class Customer {

    @Autowired
    CustomerService customerService;

    @GetMapping("/getAll")
    public GenericResponse getAllCustomers(){
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(HttpStatus.OK);
        genericResponse.setResponse(customerService.getAllCustomer());
        return genericResponse;
    }

    @DeleteMapping("/remove/{code}")
    public GenericResponse deleteCustomer( @PathVariable String code ,@RequestParam String userId) {
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(HttpStatus.OK);
        customerService.deleteCustomer(code, userId);
        genericResponse.setResponse("Deleted!");
        return genericResponse;
    }

    @GetMapping("/find/{key}")
    @ResponseBody
    public GenericResponse filterCustomer( @PathVariable String key) {
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(HttpStatus.OK);
        genericResponse.setResponse(customerService.filterCustomer(key));
        return genericResponse;
    }

    @PostMapping(value = "/save")
    @ResponseBody
    public GenericResponse saveCustomer(@RequestBody CustomerDTO customerDTO) {
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(HttpStatus.OK);
        customerService.saveCustomer(customerDTO);
        genericResponse.setResponse("saved!");
        return genericResponse;
    }

    @GetMapping("/generate")
    @ResponseBody
    public GenericResponse generateCode(){
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(HttpStatus.OK);
        genericResponse.setResponse(customerService.generateCode());
        return genericResponse;
    }

}
