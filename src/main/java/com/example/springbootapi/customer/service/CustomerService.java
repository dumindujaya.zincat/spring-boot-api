package com.example.springbootapi.customer.service;

import com.example.springbootapi.common.entity.Customer;
import com.example.springbootapi.customer.dto.CustomerDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


public interface CustomerService {
    List<CustomerDTO> getAllCustomer();

    List<CustomerDTO> filterCustomer(String param);

    void deleteCustomer(String code, String userId);

    void saveCustomer(CustomerDTO customerDTO);

    String generateCode();


}
