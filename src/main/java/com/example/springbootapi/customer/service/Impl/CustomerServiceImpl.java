package com.example.springbootapi.customer.service.Impl;

import com.example.springbootapi.common.entity.*;
import com.example.springbootapi.common.repo.*;
import com.example.springbootapi.customer.dto.ContactPersonDTO;
import com.example.springbootapi.customer.dto.CustomerDTO;
import com.example.springbootapi.customer.dto.CustomerTypeDTO;
import com.example.springbootapi.customer.dto.DocumentDTO;
import com.example.springbootapi.customer.service.CustomerService;
import jakarta.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepo customerRepo;
    @Autowired
    ContactPersonRepo contactPersonRepo;
    @Autowired
    DocumentRepo documentRepo;
    @Autowired
    LogRepo logRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    LogTypeRepo logTypeRepo;


    @Override
    public List<CustomerDTO> getAllCustomer() {
        List<Customer> customers =  customerRepo.findAll();
        ArrayList<CustomerDTO> customerDTOS = new ArrayList<>();
        if (!customers.isEmpty()){
            for (Customer c:
                    customers) {
                if (c.isAlive()){
                    customerDTOS.add(customerToDTOMapper(c));
                }
            }
        }
        return customerDTOS;
    }

    @Override
    public List<CustomerDTO> filterCustomer(String key) {
        List<Customer> customers =  customerRepo.filterCustomers(key);
        ArrayList<CustomerDTO> customerDTOS = new ArrayList<>();
        if (!customers.isEmpty()){
            for (Customer c:
                    customers) {
                if (c.isAlive()){
                    customerDTOS.add(customerToDTOMapper(c));
                }
            }
        }
        return customerDTOS;
    }

    @Transactional
    @Override
    public void saveCustomer(CustomerDTO customerDTO) {
//        first save the customer
        try {
            Customer customer = customerRepo.save(dtoToCustomerMapper((customerDTO)));
//            save each document, contact_person and log details,
        List<Document> documents =  dtoToDocumentMapper(customerDTO.getDocuments(), customer);
        if (!documents.isEmpty()){
            customer.setDocuments(documentRepo.saveAll(documents));
        }
//        for contact persons
        List<ContactPerson> contactPersonList =  dtoToContactPersonMapper(customerDTO.getContactPersons());
            if (!contactPersonList.isEmpty()){
                for (ContactPerson contactPerson:
                     contactPersonList) {
                    if(!contactPersonRepo.existsByEmail(contactPerson.getEmail())){
                        contactPersonRepo.save(contactPerson);
                    }
                }
            }
//            retrieve the user and log_type
            User user = userRepo.getUserById(customerDTO.getUserId());
            LogType logType = logTypeRepo.getLogTypeById("L003");
//            saving the log
            Log log = logRepo.save(logMapper(customer,user,logType));
//         updating relations
            user.getLog().add(log);
            logType.getLog().add(log);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    public String generateCode() {
         String prev =  customerRepo.getLastCode();
         String code = prev.substring(0,1);
         int num = Integer.parseInt(prev.substring(1,4)) + 1;
         int length = 4 - String.valueOf(num).length();
        System.out.println(length);
        return code + StringUtils.leftPad(String.valueOf(num),length,"0");
    }

    @Override
    public void deleteCustomer(String code, String userId) {
        Customer customer = customerRepo.getCustomerByCode(code);
        customer.setAlive(false);
//        creating a log
        User user = userRepo.getUserById(userId);
        LogType logType = logTypeRepo.getLogTypeById("L002");
        Log log = logRepo.save(logMapper(customer,user,logType));
        customer.getLogs().add(log);
        user.getLog().add(log);
        logType.getLog().add(log);
    }

//    contactPersonDTO --> contactPerson
    private List<ContactPerson> dtoToContactPersonMapper(List<ContactPersonDTO> contactPersonDTOS){
        List<ContactPerson> contactPersonList = new ArrayList<>();
        if (!contactPersonDTOS.isEmpty()){
            for (ContactPersonDTO dto:
                    contactPersonDTOS) {
                ContactPerson contactPerson = new ContactPerson();
                contactPerson.setId(dto.getId());
                contactPerson.setName(dto.getName());
                contactPerson.setMobile(dto.getMobile());
                contactPerson.setEmail(dto.getEmail());
                contactPerson.setDesignation(dto.getDesignation());
                contactPersonList.add(contactPerson);
            }
        }
        return contactPersonList;
    }

    private Log logMapper(Customer customer, User user, LogType logType){
        Log log = new Log();
        log.setCustomer(customer);
        log.setUser(user);
        log.setType(logType);
        return log;
    }

//    documentDTO --> Document
    private List<Document> dtoToDocumentMapper(List<DocumentDTO> documentDTOS, Customer customer){
        List<Document> documents = new ArrayList<>();
        if (!documentDTOS.isEmpty()){
            for (DocumentDTO dto:
                    documentDTOS) {
                Document document = new Document();
                document.setType(dto.getType());
                document.setName(dto.getName());
                document.setId(dto.getId());
                document.setData(dto.getData());
                document.setCustomer(customer);
                documents.add(document);
            }
        }
        return documents;
    }

//    customerDTO -->

    private CustomerDTO customerToDTOMapper(Customer customer){
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setName(customer.getName());
        customerDTO.setType(customerTypeToDtoMapper(customer.getType()));
        customerDTO.setCity(customer.getCity());
        customerDTO.setCode(customer.getCode());
        customerDTO.setGender(customer.getGender());
        customerDTO.setMobileNo(customer.getMobile());
        customerDTO.setCountry(customer.getCountry());
        return customerDTO;
    }

//    when saving
    private Customer dtoToCustomerMapper(CustomerDTO customerDTO){
        Customer customer = new Customer();
        customer.setName(customerDTO.getName());
        customer.setId(customerDTO.getId());
        customer.setNic(customerDTO.getNic());
        customer.setEmail(customerDTO.getEmail());
        customer.setType(dtoToCustomerTypeMapper(customerDTO.getType()));
        customer.setCity(customerDTO.getCity());
        customer.setRef_no(customerDTO.getRef_no());
        customer.setCompany(customerDTO.getCompany());
        customer.setAddress(customerDTO.getAddress());
        customer.setCode(generateCode());
        customer.setGender(customerDTO.getGender());
        customer.setMobile(customerDTO.getMobileNo());
        customer.setCountry(customerDTO.getCountry());
        return customer;
    }

//    customerTypeDTO --> customerType
    private CustomerType dtoToCustomerTypeMapper(CustomerTypeDTO customerTypeDTO) {
        System.out.println(customerTypeDTO);
        CustomerType customerType = new CustomerType();
        customerType.setId(customerTypeDTO.getId());
        customerType.setType(customerTypeDTO.getType());
//        resolving the list (CustomerDTO --> Customer)
        List<Customer> customers = new ArrayList<>();
        if (customerTypeDTO.getCustomerDTOs() != null){
            for (CustomerDTO dto:
                    customerTypeDTO.getCustomerDTOs()) {
                customers.add(dtoToCustomerMapper(dto));
            }
        }
        customerType.setCustomers(customers);
        return customerType;
    }

//    customerType --> customerTypeDTO
    private CustomerTypeDTO customerTypeToDtoMapper(CustomerType customerType){
        CustomerTypeDTO customerTypeDTO = new CustomerTypeDTO();
        customerTypeDTO.setId(customerType.getId());
        customerTypeDTO.setType(customerType.getType());
        return customerTypeDTO;
    }



}
